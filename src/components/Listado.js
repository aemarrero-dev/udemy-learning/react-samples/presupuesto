import React from 'react';
import Gasto from './Gasto';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Componente que recibe los gastos registrados y los envia a componente Gasto para ser mostrados.
const Listado = ({gastos}) => (
    <div className = "gastos-realizados">
         <h2>Listado de Gastos</h2>
         { gastos.map(gasto => (
            <Gasto
                key = {gasto.id}
                gasto = {gasto}
            />

         ))}
    </div>
)

Listado.propTypes = {
    gastos : PropTypes.array.isRequired
}
 
export default Listado;