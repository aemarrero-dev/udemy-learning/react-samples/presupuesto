import React from 'react';
import PropTypes from 'prop-types'; //Para documentar el codigo

const Error = ({mensaje}) =>  ( 
    <p className = "alert alert-danger error">{mensaje}</p>
 );

Error.propTypes = {
    mensaje : PropTypes.string.isRequired
}

export default Error;