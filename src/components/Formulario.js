import React, {useState} from 'react';
import Error from './Error';
import shortid from 'shortid';
import PropTypes from 'prop-types'; //Para documentar el codigo

const Formulario = ({guardarGasto, guardarCrearGasto, restante}) => {
    
    //State para almacenar nombre de gasto y cantidad gastada
    const [nombreGasto, guardarNombre] = useState('');
    const [cantidad, guardarCantidad] = useState(0);
    
    //State para validacion de cantidad ingresada por usuario
    const[error, guardarError] = useState(false);

    //Submit al agregar gasto.
    const agregarGasto = e => {
        e.preventDefault();

        //Validar datos ingresados.
        if(cantidad < 1 || isNaN(cantidad) || nombreGasto.trim() === '' || cantidad > restante ){
            guardarError(true);
            return;
        }

        guardarError(false);

        //Construir el gasto. Creamos id para identificar cada gasto en la lista de gastos
        const gasto = {
            nombreGasto,
            cantidad,
            id: shortid.generate() //instalamos npm shortid para generar id. Se pudo usar uuid.
        }
        console.log(gasto);

        //Pasar el gasto a componente principal App
        guardarGasto(gasto);
        guardarCrearGasto(true);

        //Resetear el form
        guardarNombre('');
        guardarCantidad(0);
    } 
    return ( 

        <form onSubmit = {agregarGasto}>

            <h2>Agrega tus Gastos Aqui</h2>
            {
                error 
                ? <Error mensaje = "Datos obligatorios. El Gasto no debe superar presupuesto."/>
                : null
            }
            <div className = "campo">
                <label>Nombre Gasto</label>
                <input
                    type = "text"
                    className = "u-full-width"
                    placeholder = "Ej. Transporte" 
                    value = {nombreGasto}
                    onChange = {e => guardarNombre(e.target.value)}
                />
            </div>
            <div className = "campo">
                <label>Cantidad Gasto</label>
                <input
                    type = "number"
                    className = "u-full-width"
                    placeholder = "Ej. 300" 
                    value = {cantidad}
                    onChange = {e => guardarCantidad(parseInt(e.target.value, 10))}
                />
            </div>
            <input
                type = "submit"
                className = "button-primary u-full-width"
                value = "Agregar Gasto"
            />  
        </form>
     );
}

Formulario.propTypes = {
    guardarGasto : PropTypes.func.isRequired,
    guardarCrearGasto : PropTypes.func.isRequired
}

export default Formulario;