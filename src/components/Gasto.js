import React from 'react';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Componente que muestra el gasto ingresado.
const Gasto = ({gasto}) => (
    <li className = "gastos">
        <p>
            {gasto.nombreGasto}
            <span className = "gasto">$ {gasto.cantidad}</span>
        </p>
    </li>
)

Gasto.propTypes = {
    gasto : PropTypes.object.isRequired
}

export default Gasto;