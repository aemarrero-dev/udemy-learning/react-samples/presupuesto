import React, {Fragment, useState} from 'react';
import Error from './Error';
import PropTypes from 'prop-types'; //Para documentar el codigo

//Componente que recibe presupuesto inicial ingresado por usuario
const Pregunta = ({guardarPresupuesto, guardarRestante, actualizarPregunta}) => {
    
    //State para presupuesto ingresado por usuario
    const[cantidad, guardarCantidad] = useState(0);
    //State para validacion de cantidad ingresada por usuario
    const[error, guardarError] = useState(false);
    
    //Funcion que lee presupuesto. Aplicamos parseInt ya que siempre los input devuelven string.
    const onChangeDefinirPresupuesto = e => {
        guardarCantidad(parseInt(e.target.value, 10));
        console.log(e.target.value);
    }

    //Submit para definir el presupuesto.
    const agregarPresupuesto = e => {
        e.preventDefault();
        
        //Validar presupuesto inicial
        if(cantidad < 1 || isNaN(cantidad)){
            guardarError(true);
            return;
        }

        guardarError(false);
        guardarPresupuesto(cantidad);
        guardarRestante(cantidad);
        actualizarPregunta(false); //Para que no se visualize este componente

        //Procesar los datos.
    }

    return ( 
        <Fragment>
            <h2>Coloca tu presupuesto</h2>
            
            {
                error 
                ? <Error mensaje = "El Presupuesto es Incorrecto."/>
                : null
            }
            <form onSubmit = {agregarPresupuesto}>
                <input
                    type = "number"
                    className = "u-full-width"
                    placeholder = "Coloca tu presupuesto"
                    onChange = {onChangeDefinirPresupuesto}
                    //onChange = {e =>  guardarCantidad(parseInt(e.target.value, 10));}
                /> 

                <input
                    type = "submit"
                    className = "button-primary u-full-width"
                    value = "Definir presupuesto"
                />  
            </form>
        </Fragment>
     );
}

Pregunta.propTypes = {
    guardarPresupuesto : PropTypes.func.isRequired,
    guardarRestante : PropTypes.func.isRequired,
    actualizarPregunta : PropTypes.func.isRequired
}

export default Pregunta;