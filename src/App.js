import React, {useState, useEffect} from 'react';
import Pregunta from './components/Pregunta';
import Formulario from './components/Formulario';
import Listado from './components/Listado';
import ControlPresupuesto from './components/ControlPresupuesto';

function App() {

  //State para presupuesto y restante.
  const [presupuesto, guardarPresupuesto] = useState(0);
  const [restante, guardarRestante] = useState(0);
  const [mostrarPregunta, actualizarPregunta] = useState(true);

  //Almacenara los gastos registrados.
  const [gastos, guardarGastos] = useState([]);

  const [gasto, guardarGasto] = useState({}); 

  const [creargasto, guardarCrearGasto] = useState(false);

  //Cuando agregamos un nuevo gasto.
  /*const agregarNuevoGasto = gasto => {
    guardarGastos([...gastos, gasto]);
  }*/

  //useEffect que actualiza el restante.
  //Cuando agregamos un nuevo gasto.
  useEffect(() => {
    
    if(creargasto) {
      
      if(restante > 0) {
        //Agrega el nuevo presupuesto.
        guardarGastos([...gastos, gasto]);
      }
    
      //Resta del presupuesto actual
      const presupuestoRestante = restante - gasto.cantidad;
      
      //Cuando se gasta presupuesto asi no mostrara numero negativo
      if(presupuestoRestante < 0) {
        guardarRestante(0);
      } else {
         guardarRestante(presupuestoRestante);
      }
      
      //Reseteamos a false. Esto elimina un comportamiento por default al agregar gasto
      guardarCrearGasto(false);
    }

     //Inicialmente solo tenia gasto como dependencia en el useEffect. 
     //Se agregaron los demas para eliminar los warnings en consola.
  }, [gasto, creargasto, gastos, restante]); 

  return (
    <div className = "container">
      <header>
        <h1>Gasto Semanal</h1>
          <div className = "contenido-principal contenido">
            { 
              mostrarPregunta //Ternario: Carga o visualizacion condicional de un componente.
              ? (
                  <Pregunta
                    guardarPresupuesto = {guardarPresupuesto}
                    guardarRestante = {guardarRestante}
                    actualizarPregunta = {actualizarPregunta}
                  />
                ) 
              : (
                <div className = "row">
                  <div className = "one-half column">
                    <Formulario //Formulario de gastos
                      guardarGasto = {guardarGasto}
                      guardarCrearGasto = {guardarCrearGasto}
                      restante = {restante} //Idea mia para validar
                    />
                  </div>
                  <div className = "one-half column">
                    <Listado 
                      gastos = {gastos} //Array de gastos (nombre/cantidad)
                    />

                    <ControlPresupuesto
                      presupuesto = {presupuesto}
                      restante = {restante} 
                    />
                  </div>
                </div>
                )
            }
          </div>
      </header>
    </div>
   
  );
}

export default App;

/* DEPLOYMENT DE ESTE PROYECTO
1. npm run build. Nos genera una carpeta build en nuestro proyecto.
2. Usaremos Netlify para desplegar nuestro proyecto de manera publica en la web
3. Podemos desplegar a traves de Git o copiando la carpeta build en Netlify
*/